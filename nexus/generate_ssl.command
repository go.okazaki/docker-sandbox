#!/bin/bash

cd $(dirname $0)

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ssl/ssl.key -out ssl/ssl.crt

rc=$?
if [ $rc -ne 0 ]; then
    exit $rc
fi

openssl dhparam -out ssl/dhparam.pem 2048
