AWS CLI with Session Manager Plugin
===

### 初期設定

1. Build Docker Image

```shell
cd docker/aws-cli
docker build -t aws-cli .
```

2. Install

```shell
cp -p ./aws /usr/local/bin/.
```

### ECS(Fargate) Exec 実行例

1. Get Task Id

```shell
aws ecs list-tasks --cluster <cluster> --service-name <service>
```

2. Connect Container

```shell
aws ecs execute-command --interactive --cluster dev-mase --container gateway --command /bin/ash \
--task arn:aws:ecs:<region>:<account>:task/<cluster>/<task>
```

## References

* <https://docs.aws.amazon.com/ja_jp/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html>
* <https://github.com/aws/aws-cli/issues/5373>
