#!/bin/bash

docker-compose exec runner gitlab-runner register --executor "docker" --docker-image ubuntu:latest --docker-privileged --docker-volumes "/certs/client"
