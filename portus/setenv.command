#!/bin/bash

cd $(dirname $0)

echo 'HOST_IP='$(ipconfig getifaddr en0) > .env
echo 'HOST_NAME='$(hostname) >> .env
echo 'SECRET_KEY_BASE='$(openssl rand -hex 64) >> .env
echo 'DB_PASSWORD='$(openssl rand -hex 8) >> .env
echo 'PORTUS_PASSWORD='$(openssl rand -hex 8) >> .env
