#!/bin/bash

cd $(dirname $0)

openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout certificates/portus.key -out certificates/portus.crt
