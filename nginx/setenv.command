#!/bin/bash

cd $(dirname $0)

echo 'HOST_IP='$(ipconfig getifaddr en0) > .env
echo 'HOST_NAME='$(hostname) >> .env
